HubShark - Blackpool
====================

_**Blackpool**_ is the codename of the first version of HubShark. 


## Planned Features

* **Full i18n**
     * using the _jms/translation-bundle_
* **EU Cookie Compliance**
     * using the _ xsolve-pl/xsolve-cookie-acknowledgement-bundle_
     * Note: this bundle seems to not be kept up-to-date, so we may fork this into our own bundle
* **CKEditor integration**
     * egeloen/ckeditor-bundle
* **FOSUserBundle**
* **SonataAdmin/EasyAdmin - (undecided)**
     * SonataAdmin is harder to install, can be configured nicely , is widely used
     * EasyAdmin is easy to use, not sure about modding the backend
* **Shiva's Versioning**
     * using the _shivas/versioning-bundle_
* **Static Content**
* **Blog Posts**
* **Captcha/reCaptcha**
     * Not yet decided what to use or develop our own (I don't like reinventing the wheel - somebody already done that!)
* **File Upload**
     * No real thoughts about this one
* **Syntax Highlighting**
     * Still checking possabilties on this
     * I'd like something simular to _CRAYON_ for WP
* **Contact Us page**
     * Complete integration
     * Responsive
* **Legal Pages**
     * Simular to the_Static Content_
     * Keeps things like:
          * Terms
          * License Details
          * Privacy Policy
          * Imprint
               * These are Documents that need to be amended by an ADMIN
               * These hardly ever change, maybe every 2-3 years if at all.
               * These are kept very simple - Only need Title, Content, Tags, Creation Date, Published

## How to Use

We're still in development, so we don't know how we will deploy this. Until then you could get it like this on a development server (pc?):

```
git clone https://gitlab.com/HubShark/hs-blackpool.git
```

IMPORTANT!!! Fix File Permissions (if you are not using Linux check the Symfony instructions). First make sure to change into the project’s directory:

```
cd hs-blackpool
```

Then fix the permissions like this:

```
HTTPDUSER=`ps axo user,comm | grep -E '[a]pache|[h]ttpd|[_]www|[w]ww-data|[n]ginx' | grep -v root | head -1 | cut -d\  -f1`

# if this doesn't work, try adding `-n` option
sudo setfacl -R -m u:"$HTTPDUSER":rwX -m u:`whoami`:rwX var
sudo setfacl -dR -m u:"$HTTPDUSER":rwX -m u:`whoami`:rwX var
```
The next part will ask for Databank Info (you should already have them) and at the end it will ask for setting your "secret". You can get an Online Generated key here.

```
composer install
```

answer any questions with the _enter tab_ and it will or may end with errors message about an unkown databank. 

Fix that error with this:

```
php bin/console doctrine:database:create
```

Then Create the database schema

```
php bin/console doctrine:schema:create
```

And now update your database:

```
php bin/console doctrine:schema:update --force
```
<!--
Then create your: **Admin User** (Replace "_**Admin**_" with your _Admin User_ name):

```
php bin/console fos:user:create Admin --super-admin
```
-->
And then we run this to update everything:

```
composer update --with-dependencies
```

